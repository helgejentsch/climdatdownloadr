## Welcome to the ClimDatDownloadR - Update 05.09.2023

**Today we moved the active support of the repository and further development to GitHub.com ([github.com/HelgeJentsch/ClimDatDownloadR](https://github.com/HelgeJentsch/ClimDatDownloadR)).** 

For the installation process you need to need to input the following code: 
```(r)
if(!require(devtools)) install.packages("devtools")
library(devtools)
devtools::install_github("HelgeJentsch/ClimDatDownloadR")
```

Additionally, the feedback and support e-mail address is now **helgejentsch.research@gmail.com**. 

Feedback and ideas for improving your user experience are always apprechiated!

If you want to cite ClimDatDownloadR you can find the package on Zenodo via [![DOI](https://zenodo.org/badge/DOI/10.5281/zenodo.8319614.svg)](https://doi.org/10.5281/zenodo.8319614) (or a newer version).
